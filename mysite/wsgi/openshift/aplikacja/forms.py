# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm, HiddenInput, Textarea, PasswordInput


class ComForm(forms.Form):
    slowo = forms.CharField(label=u'slowo',max_length=200, min_length=1, widget=forms.Textarea)
