# -*- coding: utf-8 -*-
import datetime
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.core.mail import send_mail
from uuid import uuid4 as uuid
from django.contrib.auth.decorators import login_required
from django.shortcuts import render , redirect
from bs4 import BeautifulSoup 
import urllib2
from models import  *
from forms import *
import re
import BeautifulSoup
Soup = BeautifulSoup.BeautifulSoup
import requests

def index(request):
    return render(request , 'aplikacja/index.html')

def wp(request):
    Wpp.objects.all().delete()
    url2 = "http://wp.pl/"
    url_opener2 = urllib2.urlopen(url2)
    page2 = url_opener2.read()
    parsed2 = Soup(page2)
    url_opener2.close()
    tabela_danych2 = parsed2.findAll('ul',{'class':'dot'})
    for j in tabela_danych2:
        z = j.findAll('li','')
        for k in z:
            desc = Wpp(description = k.text)
            desc.save()
    baza = Wpp.objects.all()
    context = {'baza': baza}
    return render(request, 'aplikacja/wp.html', context)

def interia(request):
    Interiaa.objects.all().delete()
    url = "http://interia.pl/"
    opener = urllib2.build_opener()
    url_opener = opener.open(url)
    page = url_opener.read()
    parsed = Soup(page)
    tabela_danych = parsed.findAll('ul','list news')
    for k in tabela_danych:
        z = k.findAll('li','')
        for o in z:
            h = o.find('a')
            desc = Interiaa(description = h.text)
            desc.save()
    baza = Interiaa.objects.all()
    context = {'baza': baza}
    return render(request, 'aplikacja/interia.html', context)

def onet(request):
    Onett.objects.all().delete()
    url2 = "http://onet.pl/"
    url_opener2 = urllib2.urlopen(url2)
    page2 = url_opener2.read()
    parsed2 = Soup(page2)
    url_opener2.close()
    tabela_danych2 = parsed2.findAll('span', 'newsTitle')
    print len(tabela_danych2)
    for j in tabela_danych2:
        desc = Onett(description = j.text)
        desc.save()
    baza = Onett.objects.all()
    context = {'baza': baza}
    return render(request, 'aplikacja/onet.html', context)

def wyszukane(request):
    zapytanie = request.session.get('lista')
    baza = Wyszukiwarkaa.objects.all()
    title = ''
    if baza.count() == 0:
        title = 'Nie ma zadnego newsa ...'
    else:
        title = 'Wyszukane newsy:'
    context = {'baza' : baza , 'title' : title , 'zapytanie' : zapytanie}

    return render(request , 'aplikacja/wyszukane.html', context)

def pobieranieNewsow():
    zz = []
    url = "http://interia.pl/"
    url_opener = urllib2.urlopen(url)
    page = url_opener.read()
    url_opener.close()
    parsed = Soup(page)
    tabela_danych = parsed.findAll('ul','list news')
    for k in tabela_danych:
        z = k.findAll('li','')
        for o in z:
            h = o.find('a')
            zz.append(h.text)

    url = "http://onet.pl/"
    url_opener = urllib2.urlopen(url)
    page = url_opener.read()
    parsed = Soup(page)
    url_opener.close()
    tabela_danych = parsed.findAll('span', "newsTitle")
    for j in tabela_danych:
        zz.append(j.text)

    url = "http://wp.pl/"
    url_opener = urllib2.urlopen(url)
    page = url_opener.read()
    parsed = Soup(page)
    url_opener.close()
    tabela_danych = parsed.findAll('li')

    for j in tabela_danych:
        zz.append(j.text)
    return zz

def szukaj(request):
    form = ComForm(request.POST)
    if form.is_valid():
        tak =0
        listaSlow = []
        Wyszukiwarkaa.objects.all().delete()
        slowo = form.cleaned_data.get('slowo')
        listaSlow = slowo.split()
        if ' ' in slowo:
            tak = 1
        else:
            tak = 0
        list = []
        list = pobieranieNewsow()
        if listaSlow[0][0] == '+':
            tak = 2
        if listaSlow[0][0] == '-':
            tak = 3
        g = 0
#wyszukiwanie dla kilku slow rozdzielonych spacja 
        if tak == 1 or tak == 0:
            for k in list:
                kk = []
                kk = re.sub('[^A-Za-z0-9]+', ' ', k).split()
                for j in kk:
                    for h in listaSlow:
                        if h.lower() == j.lower():
                            z = Wyszukiwarkaa(description = k)
                            z.save()




        if tak == 2:
            wyraz = listaSlow[0][1:]
            del listaSlow[0]
            listaSlow.append(wyraz)
            for k in list:
                kk = []
                kk = re.sub('[^A-Za-z0-9]+', ' ', k).split()
                for j in kk:
                    for h in listaSlow:
                        if h.lower() == j.lower():
                            z = Wyszukiwarkaa(description = k)
                            z.save()


        if tak == 3:
            wyraz = listaSlow[0][1:]
            licz =0
            del listaSlow[0]
            listaSlow.append(wyraz)
            for k in list:
                kk = []
                kk = re.sub('[^A-Za-z0-9]+', ' ', k).split()
                kombo  = len(kk)*len(listaSlow)
                for j in kk:
                    for h in listaSlow:
                        if h.lower() != j.lower():
                            licz = licz +1
                            if licz == kombo:
                                z = Wyszukiwarkaa(description = k)
                                z.save()
                licz = 0

        request.session['lista'] = slowo
        return redirect('wyszukane')


    return render(request,'aplikacja/wyszukaj.html', locals())
